var express = require('express');
var graph = require('fbgraph');
var Config = require('config-js');
var config = new Config('config.js');
var app = module.exports = express()

var conf = {
    client_id: config.get('app_id'),
    client_secret: config.get('app_secret'),
    scope: 'user_about_me, user_status',
    redirect_uri: config.get('redirect_uri')
};

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
//app.use(express.bodyParser());
//app.use(express.methodOverride());
//app.use(app.router);

// Routes

app.get('/', function(req, res) {
    res.redirect('/auth/facebook');
});

app.get('/auth/facebook', function(req, res) {
    // redirect to oauth dialog

    if (!req.query.code) {
	var authUrl = graph.getOauthUrl({
	    "client_id": conf.client_id,
	    "redirect_uri": conf.redirect_uri,
	    "scope": conf.scope
	});

	if (!req.query.error) { // checks whether a user denied the app facebook login/permiss
	    res.redirect(authUrl);
	} else { // req.query.error == 'access_denied'
	    res.send('access denied');
	}
	return;
    }

    graph.authorize({
	"client_id": conf.client_id,
	"redirect_uri": conf.redirect_uri,
	"client_secret": conf.client_secret,
	"code": req.query.code
    }, function (err, facebookRes) {
	//console.log(facebookRes.access_token)
	graph.setAccessToken(facebookRes.access_token)
	res.redirect('/UserHasLoggedIn');
    });

});


// User sent here after logging in
app.get('/UserHasLoggedIn', function(req, res) {
    console.log("logged In!");
    res.redirect('/getStatuses');
    //res.end()
});

// Scrape statuses here
app.get('/getStatuses', function(req, res) {
    var params = {fields: "statuses"};
    graph.get("/me", params, function(err, res) {
	console.log(res.statuses.data);
    });
    res.write("getting info");
    res.end();
});


var port = process.env.PORT || 8000;
app.listen(port, function() {
    console.log("Express server listening on port %d", port);
});
    
